APP_NAME   := tmp-mail
GIT_COMMIT := $(shell git rev-parse --short HEAD)

all: fmt vet lint build

.PHONY: fmt
fmt:
	go fmt ./...

.PHONY: vet
vet:
	go vet ./...
	shadow ./...

.PHONY: lint
lint:
	golint ./...

.PHONY: build
build:
	mkdir -p bin
	CGO_ENABLED=0 go build -o bin/${APP_NAME} .

.PHONY: docker-build
docker-build: build
	docker build -t ${APP_NAME} .

.PHONY: docker-push
docker-push:
	docker tag ${APP_NAME} ${DOCKER_REGISTRY}${APP_NAME}:latest
	docker tag ${APP_NAME} ${DOCKER_REGISTRY}${APP_NAME}:${GIT_COMMIT}
	docker push ${DOCKER_REGISTRY}${APP_NAME}:latest
	docker push ${DOCKER_REGISTRY}${APP_NAME}:${GIT_COMMIT}

.PHONY: clean
clean:
	rm -fr bin
