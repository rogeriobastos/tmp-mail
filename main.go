package main

import (
	"bufio"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/ReneKroon/ttlcache/v2"
	"github.com/tidwall/gjson"
)

const apiURL = "https://ipqualityscore.com/api/json/email/"

type response struct {
	Disposable bool `json:"disposable"`
}

type application struct {
	api       string
	cache     *ttlcache.Cache
	whitelist map[string]bool
}

func (app *application) checkEmail(email string) (bool, error) {
	domain := strings.Split(email, "@")[1]

	// Is it whitelisted?
	if app.whitelist[domain] {
		//log.Printf("whitelist: %s\n", domain)
		return false, nil
	}

	// Is it on cache?
	if val, err := app.cache.Get(domain); err != ttlcache.ErrNotFound {
		//log.Printf("cache hit: %s\n", domain)
		return val.(bool), nil
	}

	// Call ipqualityscore.com API
	//log.Printf("cache missing: %s\n", domain)
	resp, err := http.Get(app.api + email)
	if err != nil {
		return false, err
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return false, err
	}
	resp.Body.Close()

	disposable := gjson.Get(string(body), "disposable").Bool()
	// Default cache TTL is a hour but disposable domains are cached for 10 hours
	if disposable {
		app.cache.SetWithTTL(domain, disposable, 10*time.Hour)
	} else {
		app.cache.Set(domain, disposable)
	}

	return disposable, nil
}

func (app *application) sendError(w http.ResponseWriter, status int) {
	http.Error(w, http.StatusText(status), status)
	return
}

func (app *application) handleDisposable(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		app.sendError(w, http.StatusMethodNotAllowed)
		return
	}

	email := strings.ToLower(r.URL.Path[1:])
	check, _ := app.checkEmail(email)
	resp := response{check}

	js, err := json.Marshal(resp)
	if err != nil {
		app.sendError(w, http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

func (app *application) loadWhitelist() {
	file, err := os.Open("whitelist.txt")
	if err != nil {
		return
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		app.whitelist[scanner.Text()] = true
	}

	return
}

func newApp() *application {
	key := os.Getenv("IPQS_KEY")
	if key == "" {
		log.Fatal("environment variable IPQS_KEY is required")
	}

	app := &application{
		api:       apiURL + key + "/",
		cache:     ttlcache.NewCache(),
		whitelist: map[string]bool{},
	}
	app.cache.SetTTL(time.Duration(time.Hour))
	app.cache.SkipTTLExtensionOnHit(true)

	return app
}

func main() {
	app := newApp()
	app.loadWhitelist()

	mux := http.NewServeMux()
	mux.Handle("/disposable/", http.StripPrefix("/disposable", http.HandlerFunc(app.handleDisposable)))
	log.Println("Serving on 0.0.0.0:4000")
	err := http.ListenAndServe(":4000", mux)
	log.Fatal(err)
}
