FROM alpine:3.14
RUN addgroup --system somegroup \
    && adduser --system --ingroup somegroup --no-create-home someuser
USER someuser
WORKDIR /app
COPY bin/tmp-mail /app/
ENTRYPOINT ["/app/tmp-mail"]
