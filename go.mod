module github.com/jusbrasil/tmp-mail

go 1.16

require (
	github.com/ReneKroon/ttlcache/v2 v2.8.1
	github.com/tidwall/gjson v1.9.3
)
