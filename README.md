# tmp-mail

This is a simple API to check for disposable email addresses.
Under the hood it makes use of [ipqualityscore.com](https://www.ipqualityscore.com/free-email-validation-test) API and implements whitelist and (in-memory) cache to reduce the number external calls.

## Build and running

```
make build

# export the API key
export IPQS_KEY=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
./bin/tmp-mail
```

## How to use

Just send a `GET` request to `/disposable/email@domain.com` and get a response like this:

``` 
HTTP/1.1 200 OK
Content-Length: 20
Content-Type: application/json
Date: Tue, 28 Sep 2021 13:26:13 GMT

{
    "disposable": false
}
```
